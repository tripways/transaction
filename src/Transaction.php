<?php

namespace Hijauasri\Transaction;

use Hijauasri\Eloquent\User;
use Hijauasri\Transaction\Trip\Book;
use Hijauasri\Transaction\Trip\Save;

class Transaction
{
    public function __construct() {


    }

    public function setUser(User $user)
    {

    }

    public function save(Save $save)
    {

    }

    public function book(Book $book)
    {

    }
}