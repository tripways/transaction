<?php

namespace Hijauasri\Transaction\Payment;

use Hijauasri\Eloquent\Trip;

class Pay
{
    /**
     * @var Trip
     */
    protected $trip;

    /**
     * @var integer
     */
    protected $nominal;

    protected $currency = 'IDR';

    protected $paymentMethod;

    public function __construct(Trip $trip, int $nominal) {
    
        $this->trip = $trip;

        $this->nominal = $nominal;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    public function setNominal($nominal)
    {
        $this->nominal = $nominal;

        return $this;
    }

    public function setPaymentMethod($method)
    {
        $this->paymentMethod = $method;

        return $this;
    }

    public function discount(Discount $discount)
    {
        return $this;
    }

    public function cost(Cost $cost)
    {
        return $this;
    }
}