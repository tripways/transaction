<?php

namespace Hijauasri\Transaction\Payment;

class Method
{
    const CLICK_BCA = 'klik-bca';
    const TRANSFER = 'transfer';
    const MANDIRI_CLICK_PAY = '';
    const MANDIRI_E_CASH = '';
    const BCA_CLICK_PAY = '';
    const CREDIT_CARD = '';
    const INDOMARET = '';
    const ALFAMART = '';
    const ALFAMIDI = '';
    const CIMB_CLICK = '';
    const BRI_E_PAY = '';
}