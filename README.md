## Transaction

$transaction = new Transaction();

1. Save trip

$trip = Trip::find($id);



$transaction->setUser(\Auth::user())
    ->save(
        new Save($trip)
    );

2. Booking trip

$transaction
    ->booking(
        new Booking($trip)
    );

3. Pay

$pay = new Pay($trip, 100000);

$pay->setCurrency('IDR')
    ->setPaymentMethod('klik-bca')
    ->instance();

$transaction->pay($pay);

4. Discount or additional cost


$pay = new Pay($trip, 100000);

$pay->setCurrency('IDR')
    ->setPaymentMethod('klik-bca')
    ->discount(
        new Discount(1000)
    )
    ->cost(
        new Cost(10000)
    )
    ->instance();

$transaction->pay($pay);